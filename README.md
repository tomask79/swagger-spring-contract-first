# REST with contract first approach via Swagger #

Creating SOAP Web Services via contract first approach isn't something new. 

- You start with design of request and response XML example.
- You create XSD and JAXB stubs.
- Now follows Java Code with Spring WS annotations and previously generated JAXB as @Payload
- Now Spring generates WSDL for me and I can expose it statically eventually.

With REST services you can follow these steps as well with [Swagger](https://swagger.io) OPEN API toolset.
There are two approaches:

## To-down Swagger design ##

You create JSON or Yaml OPEN API design (something like WSDL for REST) and via Swagger-Codegen you generate server side of your endpoint
containing Swagger-Core annotations.

## Bottom-up Swagger design ##

You start with Java Code and Swagger-Core annotations.

There is a nice discussion about Swagger Best Practices at [Stackoverflow](https://stackoverflow.com/questions/37815587/swagger-best-practices)
Definitely check it out.

## To-Down REST Contract First design demo ##

Lets try simple demo. We want to design REST endpoint consuming personId from URL as path variable and return
simple JSON containing Person data (id and username). Before designing the openAPI concept I highly recommend 
to install **swagger-editor** locally:

```
  git clone https://github.com/swagger-api/swagger-editor.git
  cd swagger-editor
  npm install
  npm start
```  

Now into the swagger-editor type:

```
swagger: "2.0"

info:
  version: 1.0.0
  title: Simple API for test
  description: Simple API for persons database

schemes:
  - http
host: localhost
basePath: /persons

paths:
  /persons/{userId}:
    get:
      summary: Get a user by ID
      parameters:
        - in: path
          name: userId
          type: integer
          required: true
          description: Numeric ID of the user to get
      responses:
        '200':
          description: A User object
          schema:
            type: object
            properties:
              id:
                type: integer
                description: The user ID.
              username:
                type: string
                description: The user name.
```
Mentioned OpenAPI design is selfexplanatory I believe. For details definitely check [Swagger OpenAPI](https://swagger.io/specification/) 
specification. Lets get straight to the point, with swagger-codegen plugin you can generate matching **Spring MVC** 
server side (you can do so directly from swagger-editor).

```
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-09-17T19:08:39.569Z")

@Api(value = "persons", description = "the persons API")
public interface PersonsApi {

    @ApiOperation(value = "Get a user by ID", notes = "", response = InlineResponse200.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "A User object", response = InlineResponse200.class) })
    
    @RequestMapping(value = "/persons/{userId}", method = RequestMethod.GET)
    ResponseEntity<InlineResponse200> personsUserIdGet(@ApiParam(value = "Numeric ID of the user to get",required=true ) @PathVariable("userId") Integer userId);

}
```
where InlineResponse200 object is:

```
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-09-17T19:08:39.569Z")

public class InlineResponse200   {
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("username")
  private String username = null;

  public InlineResponse200 id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * The user ID.
   * @return id
  **/
  @ApiModelProperty(value = "The user ID.")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public InlineResponse200 username(String username) {
    this.username = username;
    return this;
  }

   /**
   * The user name.
   * @return username
  **/
  @ApiModelProperty(value = "The user name.")


  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse200 inlineResponse200 = (InlineResponse200) o;
    return Objects.equals(this.id, inlineResponse200.id) &&
        Objects.equals(this.username, inlineResponse200.username);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, username);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse200 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
```
Name of the object InlineResponse200 is autogenerated, because we didn't provide any model. Anyway,
Swagger provides posibility to create schema registry, lets add **definitions** part to our Swagger design:

```
swagger: '2.0'
info:
  version: 1.0.0
  title: Simple API for test
  description: Simple API for persons database
schemes:
  - http
host: localhost
basePath: /persons
paths:
  '/persons/{userId}':
    get:
      summary: Get a user by ID
      parameters:
        - in: path
          name: userId
          type: integer
          required: true
          description: Numeric ID of the user to get
      responses:
        '200':
          description: A User object
          schema:
            "$ref": "#/definitions/User"

definitions:
  User:
    type: object
    properties:
      id:
        type: integer
      username:
        type: string
    required:
    - id
    - username   
```

Now generated SpringMVC endpoint looks:

```
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-09-22T08:39:36.948Z")

@Api(value = "persons", description = "the persons API")
public interface PersonsApi {

    @ApiOperation(value = "Get a user by ID", notes = "", response = User.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "A User object", response = User.class) })
    
    @RequestMapping(value = "/persons/{userId}",
        method = RequestMethod.GET)
    ResponseEntity<User> personsUserIdGet(@ApiParam(value = "Numeric ID of the user to get",required=true ) @PathVariable("userId") Integer userId);

}
```
Where User object is same as InlineResponse200, but with more understandable name...

And there you go! You can start. 

Swagger is definitely atractive for me, because:

- With swagger-codegen you can generate your design into many server stubs like Spring MVC, Ruby...
- You can also generate client to your server design.
- With swagger-server you can even start your server stubs via ExpressJS.
- Works great with Microservices, you don't need Scala runtime as before anymore.
- Great support for Spring framework.
.
.
and many great stuff....

regards

Tomas