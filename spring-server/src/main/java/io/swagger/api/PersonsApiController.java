package io.swagger.api;

import io.swagger.model.User;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-09-22T08:39:36.948Z")

@Controller
public class PersonsApiController implements PersonsApi {



    public ResponseEntity<User> personsUserIdGet(@ApiParam(value = "Numeric ID of the user to get",required=true ) @PathVariable("userId") Integer userId) {
        // do some magic!
        return new ResponseEntity<User>(HttpStatus.OK);
    }

}
